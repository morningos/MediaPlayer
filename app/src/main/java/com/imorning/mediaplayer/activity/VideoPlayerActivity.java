package com.imorning.mediaplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;

import com.google.android.exoplayer2.ExoPlayer;
import com.imorning.mediaplayer.R;
import com.imorning.mediaplayer.player.video.VideoView;
import com.imorning.mediaplayer.utils.FileUtils;
import com.imorning.mediaplayer.utils.MyLog;
import com.shuyu.gsyvideoplayer.GSYVideoManager;
import com.shuyu.gsyvideoplayer.player.IjkPlayerManager;
import com.shuyu.gsyvideoplayer.player.PlayerFactory;
import com.shuyu.gsyvideoplayer.player.SystemPlayerManager;
import com.shuyu.gsyvideoplayer.utils.OrientationUtils;

import tv.danmaku.ijk.media.exo2.Exo2PlayerManager;

public class VideoPlayerActivity extends BaseActivity {
    //private static final String TAG = "VideoPlayerActivity";
    private FileUtils fileUtils;
    private VideoView videoPlayer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fileUtils = new FileUtils(VideoPlayerActivity.this);
        initView();
        initPlayer();
    }

    private void initView() {
        hideStatusBar(true);
        setContentView(R.layout.activity_video_player);
        videoPlayer = findViewById(R.id.video_player);
    }

    /**
     * hide the status bar and navigation bar
     *
     * @param hidden hide or show
     */
    private void hideStatusBar(boolean hidden) {
        ActionBar actionBar = getSupportActionBar();
        if (hidden) {
            // Hide both the navigation bar and the status bar.
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_FULLSCREEN);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            // Hide Action bar.
            // Notice:Replace getSupportActionBar() with getActionBar() if you aren't using androidx...
            if (actionBar != null) {
                actionBar.hide();
            }
        } else {
            if (actionBar != null) {
                actionBar.show();
            }
        }
    }

    private void initPlayer() {
        Intent intent = getIntent();
        if (intent.getDataString() != null) {
            MyLog.d(TAG, "initPlayer with url: " + intent.getDataString());
        }
        PlayerFactory.setPlayManager(Exo2PlayerManager.class);

        String url = fileUtils.getSdcardPath() + "video.mov";
        videoPlayer.setUp(url, true, fileUtils.getTitle(url));


        //增加封面
        ImageView imageView = new ImageView(this);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.mipmap.ic_launcher);
        videoPlayer.setThumbImageView(imageView);
        //增加title
        videoPlayer.getTitleTextView().setVisibility(View.VISIBLE);
        //设置返回键
        videoPlayer.getBackButton().setVisibility(View.VISIBLE);
        //设置旋转
        OrientationUtils orientationUtils = new OrientationUtils(this, videoPlayer);
        //设置全屏按键功能,这是使用的是选择屏幕，而不是全屏
        videoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // ------- ！！！如果不需要旋转屏幕，可以不调用！！！-------
                // 不需要屏幕旋转，还需要设置 setNeedOrientationUtils(false)
                // orientationUtils.resolveByClick();
                videoPlayer.setFullHideActionBar(true);
            }
        });
        //是否可以滑动调整
        videoPlayer.setIsTouchWiget(true);
        //设置返回按键功能
        videoPlayer.getBackButton().setOnClickListener(view -> onBackPressed());

        ///不需要屏幕旋转
        videoPlayer.setNeedOrientationUtils(false);

        videoPlayer.startPlayLogic();
    }

    @Override
    protected void onPause() {
        super.onPause();
        GSYVideoManager.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GSYVideoManager.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        GSYVideoManager.releaseAllVideos();
    }

    @Override
    public void onBackPressed() {
        if (GSYVideoManager.backFromWindowFull(VideoPlayerActivity.this)) {
            return;
        }
        super.onBackPressed();
    }
}