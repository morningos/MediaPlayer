package com.imorning.mediaplayer.activity;

import android.os.Bundle;

import com.imorning.mediaplayer.R;
import com.imorning.mediaplayer.utils.FileUtils;
import com.imorning.mediaplayer.utils.MyLog;


public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    private final String testOnlineFile = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    private FileUtils fileUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initView();
        init();
    }

    /**
     * init ui
     */
    private void initView() {
        setContentView(R.layout.activity_main);
    }

    /**
     * init data and others
     */
    private void init() {
        fileUtils = new FileUtils(MainActivity.this);
        MyLog.i(fileUtils.getSdcardPath());
    }
}