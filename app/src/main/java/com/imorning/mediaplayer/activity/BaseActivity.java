package com.imorning.mediaplayer.activity;

import android.Manifest;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.imorning.mediaplayer.utils.MyLog;
import com.permissionx.guolindev.PermissionX;
import com.permissionx.guolindev.callback.RequestCallback;

import java.util.Arrays;
import java.util.List;

public class BaseActivity extends AppCompatActivity {
    protected String TAG;

    {
        //System.loadLibrary("mediaplayer");
        TAG = getClass().getSimpleName();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        PermissionX.init(this)
                .permissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                .request(new RequestCallback() {
                    @Override
                    public void onResult(boolean allGranted, @NonNull List<String> grantedList, @NonNull List<String> deniedList) {
                        if (!allGranted) {
                            MyLog.w(getClass().getSimpleName(), "miss some permission(s): " + Arrays.toString(deniedList.toArray()));
                        }
                    }
                });
    }
}
