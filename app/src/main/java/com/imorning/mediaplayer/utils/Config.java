package com.imorning.mediaplayer.utils;

import android.content.Context;
import android.os.Environment;

import java.io.File;

public class Config {
    private static Config config;
    private final Context context;

    private Config(Context context) {
        this.context = context;
    }

    public static synchronized Config getInstance(Context context) {
        if (config == null) {
            config = new Config(context);
        }
        return config;
    }

    /**
     * get cache dir
     *
     * @return Absolute path for cache
     */
    public String getCachePath() {
        if (this.context == null) {
            throw new NullPointerException("Context can't be null");
        }
        return config.context.getExternalCacheDir().getAbsolutePath() + File.separator;

    }

    /**
     * get Absolute path of sdcard
     *
     * @return Absolute path of sdcard
     */
    public String getSdcardPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
    }


}
