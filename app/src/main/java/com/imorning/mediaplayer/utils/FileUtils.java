package com.imorning.mediaplayer.utils;

import android.content.Context;
import android.text.TextUtils;

import java.io.File;

public class FileUtils {
    private final Context context;
    private final Config config;

    public FileUtils(Context context) {
        this.context = context;
        config = Config.getInstance(context);
    }

    public String getSdcardPath() {
        return config.getSdcardPath();
    }

    public String getCacheDir() {
        return config.getCachePath();
    }

    /**
     * 获取指定文件路径的文件名
     * @see FileUtils#getTitle(File)
     * @param url 文件路径
     * @return 文件名或者 “”
     */
    public String getTitle(String url) {
        if (TextUtils.isEmpty(url)) {
            new com.shuyu.gsyvideoplayer.utils.FileUtils();
            return "";
        }
        return getTitle(new File(url));
    }

    public String getTitle(File file) {
        String tmpFileName = file.getName();
        tmpFileName = tmpFileName.substring(0, tmpFileName.lastIndexOf("."));
        return TextUtils.isEmpty(tmpFileName) ? "" : tmpFileName;
    }
}
