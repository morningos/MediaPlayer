package com.imorning.mediaplayer.utils;

import android.util.Log;

import com.imorning.mediaplayer.BuildConfig;

public class MyLog {
    private static final String TAG = "iMorning::MediaPlayer";
    private static final boolean isDebug = BuildConfig.DEBUG;


    public static void i(Object... message) {
        i(TAG, message);
    }

    public static void i(String tag, Object... message) {
        if (isDebug) {
            for (Object msg : message) {
                Log.i(tag, msg.toString());
            }
        }
    }

    public static void w(Object... message) {
        w(TAG, message);
    }

    public static void w(String tag, Object... message) {
        if (isDebug) {
            for (Object msg : message) {
                Log.w(tag, msg.toString());
            }
        }
    }


    public static void e(String tag, Object... message) {
        e(tag, null, message);
    }

    public static void e(Object... message) {
        e(TAG, null, message);
    }

    public static void e(String tag, Throwable throwable, Object... message) {
        if (isDebug) {
            for (Object msg : message) {
                if (throwable == null) {
                    throwable = new Throwable("Unknown result");
                }
                Log.e(TAG, msg.toString(), throwable);
            }
        }
    }

    public static void d(Object... message) {
        d(TAG, message);
    }

    public static void d(String tag, Object... message) {
        if (isDebug) {
            for (Object msg : message) {
                Log.d(tag, msg.toString());
            }
        }
    }
}
