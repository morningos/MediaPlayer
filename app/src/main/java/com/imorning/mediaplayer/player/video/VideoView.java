package com.imorning.mediaplayer.player.video;

import android.content.Context;
import android.util.AttributeSet;

import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;

public class VideoView extends StandardGSYVideoPlayer {
    public VideoView(Context context, Boolean fullFlag) {
        super(context, fullFlag);
    }

    public VideoView(Context context) {
        super(context);
    }

    public VideoView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
